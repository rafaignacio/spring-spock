package br.com.itau.springSpock.domains

import spock.lang.Specification

class TestIndiceMassaCorporea extends Specification {

    def "deve calcular corretamente"() {
        given: "que as informacoes foram passadas"
        def indice = new IndiceMassaCorporea()

        when: "peso igual a 100 e altura igual a 1.98m"
        def resultado = indice.Calcular(100.0, 1.98)

        then: "resultado deve ser"
        resultado == 25.507601265177023

    }
}
