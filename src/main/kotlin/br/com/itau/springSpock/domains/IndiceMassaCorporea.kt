package br.com.itau.springSpock.domains

class IndiceMassaCorporea {

    fun Calcular(peso: Double, altura : Double) : Double {
        return peso / Math.pow(altura, 2.0)
    }
}